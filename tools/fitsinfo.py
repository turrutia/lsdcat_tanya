#! /usr/bin/env python
# FILE: fitsinfo.py
# AUTHOR: C. Herenz

from astropy.io import fits
import sys

filename = str(sys.argv[1])
fits.info(filename)
