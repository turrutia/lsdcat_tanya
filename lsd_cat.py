#!/usr/bin/env python
##
# FILE: lsd_cat.py
# AUTHOR: E. C. Herenz


# lsdcat libraries 
import lib.lsd_cat_lib as lsd_cat_lib
from lib.line_em_funcs import get_timestring
from lib.line_em_funcs import read_hdu
from lib.line_em_funcs import int_or_str
from lib.cats import cats

__version__ = lsd_cat_lib.get_version()

# python standard library
import sys,os,time,datetime
import gc 
import argparse
import string
import random
import math

# science / astronomy
import numpy as np
from astropy.io import fits
from astropy.io.fits import Column
from scipy.ndimage import measurements


# get string of the commandline that was entered by the user
command = os.path.basename(sys.argv[0])
for entity in sys.argv[1:]:
    command = command+' '+entity

# TODO - fits file writing routine ... this is still clumsy, and only a workaround
def tbhdu_from_column_list(column_list):
    tbhdu = fits.BinTableHDU.from_columns(column_list)

    tbhdu.header['LSDCVER'] = __version__
    tbhdu.header['LSDCIN'] = inputfile
    tbhdu.header['LSDCINS'] = SHDU
    tbhdu.header['LSDCCMD'] = command
    tbhdu.header['THRESH'] = thresh
    tbhdu.header['CCSPLYP0'] = (ccs_p0,
                              'arcsec')
    tbhdu.header['CCSPLYP1'] = (ccs_p1,
                              'arcsec/AA')
    tbhdu.header['CCSPLYP2'] = (ccs_p2,
                                'arcsec/AA**2')
    tbhdu.header['CCSPLYL0'] = (ccs_l0,
                              'AA')
    # tbhdu.header['CCS_P3'] = (ccs_p3,
    #                           'arcsec/nm**3')
    tbhdu.header['CCLVFWHM'] = (ccl_vfwhm,
                                'km/s')
    if mfr == True:
        tbhdu.header['MFR'] = med_filt_rout
        tbhdu.header['MFRV'] = med_filt_rout_ver
        tbhdu.header['MFRIN'] = med_filt_rout_input
        tbhdu.header['MFRW'] = med_filt_rout_width
        tbhdu.header['MFRC'] = med_filt_rout_input
    if enr == True and anr == True:
        tbhdu.header['ENR'] = effnoise_gen
        tbhdu.header['ENRV'] = effnoise_gen_ver
        tbhdu.header['ENRC'] = effnoise_gen_command
        tbhdu.header['ANR'] = applyeffnoise_routine
        tbhdu.header['ANRV'] = applyeffnoise_version
        tbhdu.header['ANRC'] = applyeffnoise_command
        tbhdu.header['ANREFM'] = applyeffnoise_method

    return tbhdu
    
# command line parsing with argparse
parser = argparse.ArgumentParser(description="""
lsd_cat.py - Create a catalog of sources above a detection
                   threshold in 3D datacube containing signal and
                   variance (presumably a template cross-correlated
                   flux & variance cube from which detection signifcances
                   can be calculated).
""",
                                 epilog="""
Supported values for --tabvalues option:
I - Running ID 
ID - Grouped object ID of an entry in the catalog.
X_PEAK_SN,Y_PEAK_SN, Z_PEAK_SN - Postion of maximum (i.e. formal) detection 
                                 significance of object.
LAMBDA_PEAK_SN - same as LAMBDA_0, but for Z_PEAK_SN.
RA_PEAK_SN, DEC_PEAK_SN - RA & DEC for X_PEAK_SN, Y_PEAK_SN.

NPIX - Number of voxels above detection threshold per source.
DETSN_MAX - Formal Detection significance of the object
            (i.e. maximum S/N value of the voxels in the obj.).
BORDER - 1 if objects near the field of view border, 0 otherwise.
        (Note: Distance to border is set via --borderdist parameter 
        [default: 10 px])
        (Note: Distances are calculated wrt. to X_PEAK_SN & Y_PEAK_SN)

NOTE: All pixel coordinates in the output catalog are 0-indexed.
                                
""",formatter_class=argparse.RawDescriptionHelpFormatter)
# not used
# X0,Y0,Z0 - Coordinates of the barycenter of the detection clusters.
#                                  (unweighted). 
# RA_0,DEC_0 - Right Asscension & Declination for X0 & Y0 (in degrees).
# LAMBDA_0 -  Wavelength corresponding to Z0 (from cube header).
# X_SN,Y_SN,Z_SN - SN Weighted coordinates of the detection significances.
#                  RA_SN, DEC_SN - RA & DEC for X_SN & Y_SN.
#                  LAMBDA_SN - same as LAMBDA_0, but for Z_SN.
# X_FLUX, Y_FLUX, Z_FLUX - flux weighted barycenter.
# LAMBDA_FLUX - wavelength corresponding to Z_FLUX.
# RA_FLUX, DEC_FLUX - RA & DEC for X_FLUX, Y_FLUX.
# X_FLUX_PEAK, Y_FLUX_PEAK, Z_FLUX_PEAK - flux peak coordinate
# LAMBDA_PEAK_FLUX - wavelength corresponding to Z_FLUX_PEAK
# RA_PEAK_FLUX, DEC_PEAK_FLUX - RA & DEC for X_FLUX_PEAK & Y_FLUX_PEAK

# out_line_form = 
# dictionary for formating output lines of catalog & names of variables
# w.r.t. names in the configuration string 
# key = Name as in --tabvalues and column name in output catalog
# value = tuple
# tuple[0] = formatter string - specifying format in ASCII table
# tuple[1] = variable name
# tuple[3] = unit
# tuple[4] = FITS format key
out_line_form = {'I':               ('%8d','running_ids','','K'),
                 'ID':              ('%8d','ids','','K'),
                 'X_PEAK_SN':       ('%8d','x_sn_max','','K'),
                 'Y_PEAK_SN':       ('%8d','y_sn_max','','K'),
                 'Z_PEAK_SN':       ('%8d','z_sn_max','','K'),
                 'RA_PEAK_SN':      ('%12.5f','ra_sn_max','deg','D'),
                 'DEC_PEAK_SN':     ('%12.5f','dec_sn_max','deg','D'),
                 'LAMBDA_PEAK_SN':  ('%10.2f','lambda_sn_max','Angstrom','D'),
                 'NPIX':            ('%6d','npix','','K'),
                 'DETSN_MAX':       ('%12.6f','det_sn_max','','D'),
                 'BORDER':          ('%6d','border_bit','','L')
                 }
                 # 'LAMBDA_FLUX':     ('%10.2f','lambda_flux_com','Angstrom','D'),
                 # 'X_FLUX_PEAK':      ('%8d','x_flux_max','','K'),
                 # 'Y_FLUX_PEAK':      ('%8d','y_flux_max','','K'),
                 # 'Z_FLUX_PEAK':      ('%8d','z_flux_max','','K'),
                 # 'RA_PEAK_FLUX':     ('%12.6f','ra_flux_max','deg','D'),
                 # 'DEC_PEAK_FLUX':    ('%12.6f','dec_flux_max','deg','D'),
                 # 'LAMBDA_PEAK_FLUX': ('%10.2f','lambda_flux_max','Angstrom','D'),


parser.add_argument("-i","--input",
                    required=True,type=str,
                    help="""
Name of the input FITS file containing either detection significance or matched filtered data and propagated variances.
""")
parser.add_argument("-S","--SHDU",
                    type=int_or_str,default='0',
                    help="""
HDU number (0-indexed) or name of input S/N or filtered cube FITS file that contains the detection significances or matched filtered data. If no `--NHDU` (see below) is supplied, we assume that the input cube in this HDU is S/N, otherwise we assume that it contains the matched filtered data. [Default: 0]
""")
parser.add_argument("-N","--NHDU",
                    type=int_or_str,
                    default=None,
                    help="""
HDU number (0-indexed) or name of filtered cube FITS file that contains the propagated variances of the matched filtering operation. [Default: not set - in this case the datacube in `--SHDU` is interpreted as S/N]
""")
parser.add_argument("-e","--expmap",
                    type=str,
                    default=None,
                    help="""
FITS file containing a 2D array where the number of exposed voxels are stored for each spaxel.  The tool `fov_map_from_expcube.py` can create such a map.  This exposure map is required if the output parameter `BORDER` is demanded in the `--tabvalues` option (see below). [Default: None]
""")
parser.add_argument("-t",
                    "--thresh",
                    default=8.0,
                    type=float,
                    help="""
Detection threshold [Default: 8.0]
""")
parser.add_argument("-c","--catalogue",
                    type=str,
                    default=None,
                    help="""
Filename of the output catalogue. Two files will be written on disc, an ASCII catalogue with the specified filename, and an FITS table with `.fits` being appended to this filename. [Default: `catalog_+INPUT+.cat`, where `INPUT` is the name of the input FITS file.]
""")
parser.add_argument("--tabvalues",
                    type=str,
                    default=None,
                    help="""
Comma-separated list of columns to be written in output catalogue. See below for a list of supported values. [Default: `I,ID,X_PEAK_SN,Y_PEAK_SN,Z_PEAK_SN,DETSN_MAX`]
""")
parser.add_argument("-r","--radius",
                    type=float,
                    default=0.8,
                    help="""
Grouping radius in arcsec.  Detections at similar spatial positions within this search radius get assigned the same `ID`. [Default: 0.8 arcsec]
""")
parser.add_argument("--spaxscale",
                    type=float,
                    default=0.2,
                    help="""
Spatial extent of a spatial pixel in arcsec. [Default: 0.8 arcsec]
                    """)
parser.add_argument("--borderdist",
                    type=float,
                    default=10.,
                    help="""
Flag detection in catalogue (column `BORDER`) if it is less than `BORDERDIST` pixels near the field of view border. Only has an effect if the field `BORDER` is requested in `--tabvalues` and if an `--expmap` is supplied. [Default: 10]
""")
parser.add_argument("--clobber",
                    action='store_true',
                    help="""
Overwrite already existing output file! 
USE WITH CAUTION AS THIS MAY OVERWRITE YOUR RESULTS!
""")

# parser.add_argument("--fluxhdu",type=int,default=1,help="""
# HDU number (0-indexed) of flux cube FITS file, which contains the 
# flux data. (default: 0)
# """)
# parser.add_argument("--fitstable",action='store_true',help="""
# Store output catalog also as FITS-Table.
# """)
# parser.add_argument("--groupcoords",type=str,default='none',help="""
# Coordinates for spatial grouping. Possible values: 
# 'sn_weighted', 'sn_max', 'flux_weighted'
# (default: no grouping).
# """)
# --- DEPRECATED --- SPILL OUT A WARNING ABOUT FUTURE REMOVAL!!
parser.add_argument("--fitstable",help=argparse.SUPPRESS,
                    action='store_true')


args = parser.parse_args()

inputfile = args.input
SHDU = args.SHDU

# originalcube = args.fluxcube
# fluxhdu = args.fluxhdu

if args.catalogue == None:
    output_catalog = 'catalogue_'+inputfile+'.cat'
else:
    output_catalog = args.catalogue

# we always write a also fits catalog ->
# remove last extension (e.g. .cat) and replace by .fits
oc = output_catalog.split('.')
output_catalog_fits = ''
for oc_i in oc[:-1]:
    output_catalog_fits += oc_i + '.'
output_catalog_fits += 'fits'
        
    
# group_coords =  args.groupcoords    
    
if os.path.isfile(output_catalog) and args.clobber == False:
    print('ERROR: output file '+output_catalog+' already exists!'+\
          ' Aborting! Use --clobber switch to force overwrite.')
    sys.exit(2)

radius = args.radius
thresh = args.thresh

spaxscale = args.spaxscale

if args.tabvalues == None:
    tabvalues = ['ID','X_PEAK_SN','Y_PEAK_SN','Z_PEAK_SN','NPIX','DETSN_MAX']
else:
    tabvalues = args.tabvalues.split(',')

# test if all entries in --tabvalues are recognised
lsd_cat_lib.tabvalue_test(tabvalues, out_line_form)

# 'BORDER' in tabvalues, but no expmap file given
if 'BORDER' in tabvalues and args.expmap == None:
    print("ERROR: 'BORDER' requested in --tabvalues, but no --expmap"+\
          " supplied. ABORTING!")
    sys.exit(2)
   
# X_PEAK_SN, Y_PEAK_SN, Z_PEAK_SN need to be present, otherwise
# lsd_cat_measure.py will not work...
if not [True,True,True] == \
   [True for element in ['X_PEAK_SN','Y_PEAK_SN','Z_PEAK_SN'] if element in tabvalues]:
    print(inputfile+': WARNING - X_PEAK_SN,Y_PEAK_SN,Z_PEAK_SN not given in --tabvalues - '+\
          'lsd_cat_measure.py will not work properly on this catalog!')

# starting message
starttime = time.time()
#print( __file__.split('/')[-1]+' version '+__version__)

# required header keywords in SHDU
header = fits.getheader(inputfile, SHDU)
try:
    ccs_p0 = header['CCSPLYP0']
    ccs_p1 = header['CCSPLYP1']
    ccs_p2 = header['CCSPLYP2']
    ccs_l0 = header['CCSPLYL0']
#    ccs_p3 = header['CCSPLYP3']
    ccs_fct = header['CCSFILT']
    ccl_vfwhm = header['CCLVFWHM']
    keywords_present = True
except KeyError:
    print(inputfile+': WARNING: FITS header keywords from matched filtering with '+\
          'lsd_cc_spectral.py & lsd_cc_spatial.py not '+\
          'present. Hence we can write no information about them in the output catalogue.')
    keywords_present = False

wavel_unit = header['CUNIT3']
try:
    wavel = lsd_cat_lib.wavel(header)  # wavelength axis array wavel_unit
except KeyError:
    # try CDELT 3  - if this fails we crash
    try:
        wavel = lsd_cat_lib.wavel(header,cdel_key='CDELT3')
    except KeyError:
        print('NO VALID WAVELENGTH INCREMENT KEYWORD FOUND IN HEADER OF INPUT CUBE (CD3_3 OR CDELT). ABORT!')
        sys.exit(2)


# header keywords - commands from additional LSDCat tools - will be
# written in header of output catalog
if 'MFR' in header:
    mfr = True
    med_filt_rout = header['MFR']
    med_filt_rout_width = header['MFRW']
    med_filt_rout_cmd = header['MFRC']
    med_filt_rout_ver = header['MFRV']
    med_filt_rout_input = header['MFRIN']
    med_filt_rout_input_shdu = header['MFRINS']
    med_filt_rout_input_nhdu = header['MFRINN']
else:
    mfr = False


if 'ENR' in header:
    # only a subset of the most important keys
    enr = True
    effnoise_gen = header['ENR']
    effnoise_gen_ver = header['ENRV']
    effnoise_gen_command = header['ENRC']
    effnoise_gen_input = header['ENRIN']
    effnoise_gen_cat = header['ENRCAT']
    effnoise_gen_appnum = header['ENRNUM']
    effnoise_gen_apprad = header['ENRRAD']
else:
    enr = False


if 'ANR' in header:
    anr = True
    applyeffnoise_routine = header['ANR']
    applyeffnoise_version = header['ANRV']
    applyeffnoise_command = header['ANRC']
    applyeffnoise_input_file = header['ANRIN']
    applyeffnoise_effnoise_file = header['ANRNF']
    applyeffnoise_method = header['ANREFM']
else:
    anr = False

print(' ... LSDCat Version '+str(__version__)+' ... ')
print(cats[0])
    
if args.fitstable:
    print('WARNING: --fitstable switch is depracted and should not be used anymore.'+\
          ' A FITS file is always created.')


# reading in datacube
if args.NHDU == None:
    print(inputfile+': Opening datacube with formal detection '+\
          'significances ... (HDU '+str(SHDU)+') '+\
          get_timestring(starttime))
else:
    print(inputfile+': Openening datacube with filtered flux values '+\
          '(HDU '+str(SHDU)+') '+\
          get_timestring(starttime))
det_sn_cube, header = read_hdu(inputfile, SHDU,
                               memmap=False, nans_to_value=True)
if args.NHDU != None:
    # det_sn_cube is filtered flux and in args.NHDU are the propagated
    # variances - in this case we calculate the det_sn_cube on the fly:
    print(inputfile+': Openening datacube with propagated variances '+\
          '(HDU '+str(args.NHDU)+') '+\
          get_timestring(starttime))
    variance_cube, var_header = read_hdu(inputfile, args.NHDU,
                                         memmap=False, nans_to_value=False)
    det_sn_cube = det_sn_cube / np.sqrt(variance_cube)

    
# detection - using scipy.measurements (label & find_objects)
print(inputfile+': Detection using threshold '+str(thresh)+\
          ' ... '+get_timestring(starttime))

labels = measurements.label(det_sn_cube > thresh,
                            structure=None)  # 6-connected topology
label_cube = labels[0]
max_label = labels[1]
objects = measurements.find_objects(label_cube)

print(inputfile+\
     ': There are %d detections above a ' %(max_label)+\
     'detection threshold of %2.2f ' %(thresh) +\
     get_timestring(starttime))

ids = np.asarray(range(1,max_label+1))  # all labels of detections


# all information for head of the output catalog is now present...
# TODO: all this header information should also be present in the fits
# catalogs at some point
cat_file = open(output_catalog,'w')
cat_file.write('# Catalog of detections in '+inputfile+' \n')
if args.NHDU == None:
    cat_file.write('# S/N HDU = '+str(SHDU)+' \n')
else:
    cat_file.write('# Matched filtered data HDU: '+str(SHDU)+' \n')
    cat_file.write('# Propagated variances HDU: '+str(args.NHDU))

if mfr == True:
    cat_file.write('# '+inputfile+' median filter subtracted with '+\
                   med_filt_rout+' version '+str(med_filt_rout_ver)+'\n')
    cat_file.write('# Median Filter Command: '+med_filt_rout_input+'\n')
    cat_file.write('# Median Filter Width: '+\
                   str(med_filt_rout_width)+'\n')


if enr == True and anr == True:
    cat_file.write('# '+inputfile+' includes effective noise'+\
                   ' generated with '+effnoise_gen+\
                   ' (v'+effnoise_gen_ver+')'+' & applied with '+\
                   applyeffnoise_routine+' (v '+applyeffnoise_version+\
                   ')'+'\n')
    cat_file.write('# Effective Noise Generation Command: '+\
                   effnoise_gen_command+'\n')
    cat_file.write('# Effective Noise Application Command: '+\
                   applyeffnoise_command+'\n')
    cat_file.write('# Effective Noise Application Method: '+\
                   applyeffnoise_method+'\n')


    
# Cross correlation paramters
if keywords_present:
    cat_file.write('# Cross correlation polynomial coefficents & zero-point: '+\
                   'p0='+str(ccs_p0)+' arcsec  p1='+str(ccs_p1)+' arcsec / AA'+\
                   ' p2='+str(ccs_p2)+' arcsec / AA^2 ; l0='+str(ccs_l0)+' \n')
    cat_file.write('# Cross correlation velocity: '+str(ccl_vfwhm)+\
                   ' km/s \n')

cat_file.write('# Threshold: '+str(thresh)+' \n')
cat_file.write('# Detections: '+str(len(ids))+' \n')
cat_file.write('# Spatial grouping radius: '+str(radius)+'" \n')
cat_file.write('# Generated: '\
               +str(datetime.datetime.now())[:-7]+' \n')
cat_file.write('# Tool: '+ __file__.split('/')[-1]+\
               ' version '+__version__+' \n')
cat_file.write('# Command: '+command+' \n')
if max_label == 0:
    print(inputfile+': No objects to analyse. Empty catalog '+\
          'written to disk: '+output_catalog+\
          ' - Quitting now gracefully!')
    # write empty fits table, if desired:
    # TODO - make this more elegant, as some of the used code here
    # is again repeated at the end
    column_list = []
    for tabvalue,var,unit,entry_format in zip(tabvalues,
                                              unit_list,
                                              fits_format_list):
        column_list.append(Column(name=tabvalue,
                                  unit=unit,
                                  array=None,
                                  format=entry_format))
        tbhdu = tbhdu_from_column_list(column_list)
        tbhdu.writeto(output_catalog_fits,clobber=args.clobber)

    print(cats[1])
    sys.exit(0)
    
# ... MEASUREMENTS on detected objects ...
# (those which are requested with the --tabvalues option)

# number of voxels per object
if 'NPIX' in tabvalues:
    print(inputfile+': Calculating number of voxels per '+\
          'object above the detection threshold ... '+\
          get_timestring(starttime))
    npix_ids, npix = lsd_cat_lib.calc_npix(label_cube, objects)


# S/N MAX
print(inputfile+': Calculating SN extrema & coordinates ... '+\
              get_timestring(starttime))
det_sn_max, x_sn_max, y_sn_max, z_sn_max = \
                                lsd_cat_lib.calc_maxima(det_sn_cube,
                                                        label_cube,
                                                        objects)
# RA / DEC / LAMBDA corresponding to S/N MAX
lambda_sn_max = lsd_cat_lib.calc_lambda(z_sn_max,
                                        header)

ra_sn_max, dec_sn_max = lsd_cat_lib.pix_to_radec(x_sn_max,
                                                 y_sn_max,
                                                 header)


# dermination of border flag (dev 1.0.2dev):
if 'BORDER' in tabvalues:
    border_dist = args.borderdist
    print(inputfile+': Flagging Objects '+str(border_dist)+'px '+\
          'near the field of view border using exposure count map '+\
          args.expmap+'... '+get_timestring(starttime))
    expmap_hdu = fits.open(args.expmap)
    expmap = expmap_hdu[0].data  # implicitly assuming it comes from
                                 # fov_map_from_expcube.py
    assert expmap.shape == det_sn_cube[0,:,:].shape
    no_exps_map = expmap == 0  # this defines our border

    indices_y, indices_x = np.indices(expmap.shape)

    # full mask contains how many pixel:
    full_sum = math.pi * border_dist**2
    full_sum = int(math.floor(full_sum))

    # now checking if these pixels are overlapping with FoV border for
    # each object
    border_bit = []
    for x_i, y_i in zip(x_sn_max,y_sn_max):
        X = indices_x - x_i
        Y = indices_y - y_i
        pixrad =  np.sqrt(X**2 + Y**2)
        pixrad_sel = pixrad <= border_dist
        mask_check = pixrad_sel.astype(int) - no_exps_map.astype(int)
        mask_check = mask_check == 1

        if mask_check.sum() < full_sum:
            # detection near border - flag it
            border_bit.append(1)
        else:
            # detection OK
            border_bit.append(0)

    border_bit = np.asarray(border_bit)
    num_border = np.sum(border_bit)
    print(inputfile+': There are '+str(num_border)+\
          ' detections close to the border...'+get_timestring(starttime))
            

    
# calculate flux releated values
# if flux_values_demanded:
#     print(inputfile+': Flux related values are demanded - loading'+\
#           ' fluxcube '+originalcube+' (Flux HDU '+str(fluxhdu + 1)+\
#           ')'+get_timestring(starttime))
#     # freeing some memory
#     del det_sn_cube
#     gc.collect()

#     # loading flux and variance
#     flux_cube, flux_cube_header = read_hdu(originalcube, fluxhdu,
#                                            memmap=False,
#                                            nans_to_value=True)

#     # everything OK?
#     if type(flux_cube) == type(None):
#         print("ERROR: Flux values demanded but "+\
#               "Flux Cube HDU contains no data. "+\
#               "Check --fluxhdu option!")
#         sys.exit(2)
#     if flux_cube.shape != label_cube.shape:
#         print('ERROR: FLUX CUBE SHAPE DOES NOT MATCH SHAPE OF '+\
#               'DET_SN_CUBE - ABORTING!')
#         print(cats[0])
#         sys.exit(2)

        
#     # peak flux
#     flux_extrema_list = ['X_FLUX_PEAK', 'Y_FLUX_PEAK', 'Z_FLUX_PEAK',
#                         'RA_PEAK_FLUX', 'DEC_PEAK_FLUX',
#                         'LAMBDA_PEAK_FLUX']
#     if any([True for element in flux_extrema_list
#             if element in tabvalues]):
#         print(inputfile+': Calculating flux extrema coordinates ...'+\
#               get_timestring(starttime))
#         x_flux_max, y_flux_max, z_flux_max = lsd_cat_lib.calc_max_win(
#             x_min, x_max, y_min, y_max, z_min, z_max, flux_cube)
#         lambda_flux_max = lsd_cat_lib.calc_lambda(z_flux_max,header)
#         ra_flux_max, dec_flux_max = lsd_cat_lib.pix_to_radec(
#             x_flux_max, y_flux_max, header)

# spatial grouping
print(inputfile+': Spatially group objects (e.g. assign same '+\
          'ID to objects  within '+str(radius)+' arcseconds) '+\
          get_timestring(starttime))

for x_sn_max_i, y_sn_max_i in zip(x_sn_max, y_sn_max):
    distances = (x_sn_max_i - x_sn_max)**2 + \
                (y_sn_max_i - y_sn_max)**2
    select = distances <= (radius/spaxscale)**2
    # detections within search radius all obtain the smallest id
    # of a detection within the radius
    ids[select] = np.min(ids[select])

# renumber detections, so that all ids from 1 to
# max_id increment by 1
new_ids = ids.copy()
for new_id, old_id in enumerate(sorted(set(ids)), 1):
    new_ids[ids == old_id] = new_id


# sort array according to ID (primary criterion) and z_coord
# (secondary criterion)
sort_array = np.zeros((len(ids),),
                      dtype=[('ids', ids.dtype),
                             ('z_sn_max', z_sn_max.dtype)]) 

sort_array['ids'] = new_ids
sort_array['z_sn_max'] = z_sn_max
    
index_sort = np.argsort(sort_array,order=('ids','z_sn_max'))

ids = new_ids

print(inputfile+': The catalog contains '+str(ids.max())+' individual objects ...')


# generate running IDs
if 'I' not in tabvalues:
    tabvalues = ['I'] + tabvalues  # we always create running IDs 
running_id_order = np.arange(1,len(ids)+1,1)
#if group_coords != 'none':
running_ids = np.zeros_like(running_id_order)
running_ids[index_sort] = running_id_order
# else:
#     running_ids = running_id_order


# ..... WRITING ENTRIES INTO OUTPUT CATALOG ......
print(inputfile+': Starting to write entries into '+output_catalog+\
      ' ASCII output catalog: '+get_timestring(starttime))
for entry in zip(range(len(tabvalues)),tabvalues):
    if out_line_form[entry[1]][2] == '':
        cat_file.write('#\t '+str(entry[0]+1)+': '+entry[1]+'\n')
    else:
        cat_file.write('#\t '+str(entry[0]+1)+': '+entry[1]+\
                       ' ['+out_line_form[entry[1]][2]+'] \n')

# Writing catalog to ascii table by generating output line string &
# variable tuple using dictionary provided at the beginning of this
# script.
#
#   example:
#
#    tabvalues = ['ID','X_SN','Y_SN','Z_SN']
#
#  -> code below makes then the following:
#
# -> line_string = '%8d %10.2f %10.2f %10.2f'
#    var_list = [ids,x_sn_com,y_sn_com,z_sn_com] 
#
# where list entries in var_list are alway numpy arrays we calculated
# above. When grouping was requested, these arrays also get sorted
# accordingly.
line_string = ''
var_list = []
unit_list = []
fits_format_list = []
for entry in tabvalues:
    entry_unit = out_line_form[entry][2]
    entry_format = out_line_form[entry][3]
    line_string += out_line_form[entry][0]+' '
    var_list.append(vars()[out_line_form[entry][1]])
    unit_list.append(entry_unit)
    fits_format_list.append(entry_format)
#    if group_coords != 'none':
    var_list[-1] = var_list[-1][index_sort]
line_string += '\n'  # end of line 

# with line_string and var_list we now can write each entry with the
# desired values as lines in the output catalog
for l in range(len(ids)):
    var_tuple_list = []
    for j in range(len(var_list)):
        var_tuple_list.append(var_list[j][l])
    var_tuple = tuple(var_tuple_list)
    cat_file.write(line_string %(var_tuple))

# and we are done with the ASCII catalog ....
cat_file.close()
print(inputfile+': Wrote output catalog: '+output_catalog+' '+\
          get_timestring(starttime))

# ... now finally - the FITS catalog
column_list = []
for tabvalue,var,unit,entry_format in zip(tabvalues,
                                          var_list,unit_list,
                                          fits_format_list):
    column_list.append(Column(name=tabvalue,
                              unit=unit,
                              array=var,
                              format=entry_format)) 

tbhdu = tbhdu_from_column_list(column_list)
tbhdu.writeto(output_catalog_fits, clobber=args.clobber)

print(inputfile+': Wrote FITS catalog '+output_catalog_fits+' '+\
          get_timestring(starttime))



print(cats[2])   # ;-)

